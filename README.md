# Docker Collection

## Getting Started

```bash
docker pull ubuntu
```

or

```bash
docker pull ubuntu:20.04
```

## Build with Dockerfile

Prepare `Dockerfile` > Build and run.

```bash
docker build -t ContainerName .
docker run -it ContainerName
```

Mount a volume (directory) in container to your host

```bash
docker run -v C:/:/mnt/c -it ContainerName
```

** Replace `ContainerName` to your preferred container name.

```mermaid
flowchart TD
    W[Host]

    subgraph Container
        C1[Container 1]
        C2[Container 2]
        C3[Container 3]
        C4[...]
        C5[Container N]
    end

    Container --> W
    C1 <-.-> |mount C:| W
```

see https://docs.docker.com/reference/cli/docker/container/run/


## How to Build C++ Code

```bash
g++ -o book.exe book.cpp
```

## Remote Ubuntu Desktop

```bash
docker build -t ubuntu:xrdp24 -f Dockerfile.ubuntu.xrdp
docker run -d -p 33890:3389 -it ubuntu:xrdp24
```

At Windows RDP > localhost::33890 > test & 1234

see https://anduin.aiursoft.cn/post/2024/6/11/run-linux-desktop-experience-in-docker-container


## Remote Ubuntu with Persistent Storage

see https://github.com/fcwu/docker-ubuntu-vnc-desktop

```bash
docker run -p 6080:80 -v /dev/shm:/dev/shm dorowu/ubuntu-desktop-lxde-vnc
```

Then launch your browser http://localhost:6080/


## References
1. https://stackoverflow.com/a/23455537/707752
2. https://askubuntu.com/q/1297571
3. https://docs.docker.com/reference/cli/docker/container/run/
